;;;;;;;;;;;;;;;;
;; Defaults
;;;;;;;;;;;;;;;;
(load-theme 'modus-operandi-tinted t)
(set-face-attribute 'header-line nil :box "#000000")

(setq-default
 indent-tabs-mode nil
 tab-width 4
 header-line-format '(:eval (display-headerline))
 mode-line-format '(:eval (display-modeline))
 line-spacing 0.2)

(setq
 default-frame-alist '((undecorated . t))
 initial-major-mode 'fundamental-mode
 display-line-numbers-type 'relative
 sentence-end-double-space nil
 backup-inhibited t
 auto-save-default nil
 compilation-ask-about-save nil
 compilation-scroll-output 'first-error
 eglot-ignored-server-capabilities '(:inlayHintProvider)
 eglot-events-buffer-config '(:size 0 :format short)
 isearch-lazy-count t
 gud-highlight-current-line t
 bookmark-save-flag 1
 Man-notify-method 'newframe
 completion-auto-select 'second-tab
 imenu-flatten 'annotation
 inhibit-startup-message t)

(add-to-list 'default-frame-alist '(font . "Iosevka Fixed SS14-14"))
(set-fontset-font t 'emoji "Noto Color Emoji")
(blink-cursor-mode -1)
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(global-auto-revert-mode 1)
(global-display-line-numbers-mode 1)
(delete-selection-mode 1)
(electric-pair-mode 1)
(winner-mode 1)
(fido-vertical-mode 1)
(add-hook 'before-save-hook #'delete-trailing-whitespace)
(add-hook 'compilation-filter-hook #'ansi-color-compilation-filter)

(use-package dired
  :hook (dired-mode . hl-line-mode)
  :config
  (setq dired-recursive-copies 'always
        dired-recursive-deletes 'always
        dired-auto-revert-buffer t
        delete-by-moving-to-trash t
        dired-dwim-target t
        dired-kill-when-opening-new-dired-buffer t
        dired-listing-switches "-hval --group-directories-first"))

(defun display-headerline ()
  (let* ((prefix (if-let* ((path (buffer-file-name))
                           (path-abb (abbreviate-file-name path))
                           (project (project-current))
                           (path-root (project-root project))
                           (path-prefix (replace-regexp-in-string "[^/]+/$" "" path-root)))
                     (string-remove-prefix path-prefix path-abb)
                   (or path-abb "%b")))
         (final-prefix (format "🗁%s ➤ " prefix)))
    (if (null which-function-mode)
        final-prefix
      `(which-func-mode (,final-prefix which-func-format)))))

(defun display-modeline ()
  `("%e"
    mode-line-modified
    " "
    mode-line-buffer-identification
    " "
    mode-line-position
    (vc-mode vc-mode)
    "  "
    ,(capitalize (string-replace "-mode" "" (symbol-name major-mode)))
    mode-line-process
    ))

(defun md-preview (arg)
  (interactive (list (read-file-name "Preview file:")))
  (let ((tmp-file (format "/tmp/%s.html" (file-name-base arg))))
    (shell-command (format "pandoc --from markdown-yaml_metadata_block --resource-path=.:img  %s -o %s" arg tmp-file))
    (eww-open-file tmp-file)))

(defun delete-word (&optional arg)
  (interactive "p")
  (delete-region (point) (progn (forward-word (or arg 1)) (point))))

(defun backward-delete-word (&optional arg)
  (interactive "p")
  (delete-word (- (or arg 1))))

(defun keyboard-quit-dwim ()
  "Do-What-I-Mean behaviour for a general 'keyboard-quit'"
  (interactive)
  (cond
   ((region-active-p)
    (keyboard-quit))
   ((derived-mode-p 'completion-list-mode)
    (delete-completion-window))
   ((> (minibuffer-depth) 0)
    (abort-recursive-edit))
   (t
    (keyboard-quit))))

(use-package font-lock
  :custom-face
  (font-lock-type-face ((t (:foreground unspecified))))
  (font-lock-constant-face ((t (:foreground unspecified))))
  (font-lock-string-face ((t (:foreground unspecified :inherit 'font-lock-doc-face))))
  (font-lock-variable-name-face ((t (:foreground unspecified)))))

(use-package grep
  :config
  (grep-apply-setting
   'grep-find-command '("rg -n -H --no-heading -i -e '' $(git rev-parse --show-toplevel || pwd)" . 30)))

(use-package ediff
  :hook
  (ediff-before-setup . (lambda() (select-frame (make-frame))))
  (ediff-quit . delete-frame)
  :init
  (setq ediff-split-window-function 'split-window-horizontally
        ediff-window-setup-function 'ediff-setup-windows-plain))

;;;;;;;;;;;;;;;;
;; Programming
;;;;;;;;;;;;;;;;
(defun gdb-new-frame ()
  (interactive)
  (select-frame (make-frame))
  (tool-bar-mode 1)
  (call-interactively 'gud-gdb))

(use-package dape
  :hook (dape-display-source . pulse-momentary-highlight-one-line)
  :bind (:map dape-global-map ("u" . dape-until-gdb))
  :config
  (dape-breakpoint-global-mode)
  (setq dape-buffer-window-arrangement 'right)

  (defun dape-until-gdb ()
    (interactive)
    (dape-evaluate-expression
     (dape--live-connection 'last)
     (format "until %d" (line-number-at-pos)))))

(with-eval-after-load 'eglot
  (dolist (lsp '((typst-ts-mode . ("tinymist"))
                 (wgsl-ts-mode . ("wgsl_analyzer" "--stdio"))))
    (add-to-list 'eglot-server-programs lsp)))

(use-package rust-ts-mode
  :mode ("\\.rs\\'" . rust-ts-mode)
  :config
  ;; https://github.com/rust-lang/rust-mode/blob/master/rust-compile.el
  (require 'rust-compile))

(use-package go-ts-mode
  :mode
  ("\\.go\\'" . go-ts-mode)
  ("go.mod"   . go-mod-ts-mode)
  :config
  (setq go-ts-mode-indent-offset tab-width))

(use-package elixir-ts-mode
  :hook (elixir-ts-mode . abbrev-mode)
  :mode
  ("\\.ex\\'" . elixir-ts-mode)
  ("\\.exs\\'" . elixir-ts-mode)
  :bind (:map elixir-ts-mode-map ("C-c t" . mix-run-test))
  :config
  (add-to-list 'compilation-error-regexp-alist 'elixir)
  (add-to-list 'compilation-error-regexp-alist-alist
               (cons 'elixir (list "└─ \\(.+\\):\\([0-9]+\\):\\([0-9]+\\)" 1 2 3)))

  (define-abbrev-table 'elixir-ts-mode-abbrev-table
    '(("de"  "" skel-ex-doend)
      ("dfm" "" skel-ex-defmodule)))

  (define-skeleton skel-ex-doend
    "Insert do-end" nil
    "do" \n
    > _ \n
    -2 "end")

  (define-skeleton skel-ex-defmodule
    "Insert defmodule-do-end" nil
    (((replace-regexp-in-string "_" "" (capitalize (file-name-base buffer-file-name))))
     "defmodule " _ str " do" \n
     \n
     "end"))

  (defun mix-run-test (&optional arg)
    "If arg is 1 it will pass the line number to mix test."
    (interactive "p")
    (let ((default-directory (project-root (project-current)))
          (current-file (buffer-file-name))
	      (current-line (line-number-at-pos)))
      (if (= arg 1)
          (compile (format "mix test %s:%s" current-file current-line))
        (compile (format "mix test %s" current-file)))))
  )

(use-package inf-elixir
  ;; https://github.com/J3RN/inf-elixir
  :load-path "non-elpa"
  :after elixir-ts-mode
  :bind
  (("C-c i i" . 'inf-elixir)
   ("C-c i p" . 'inf-elixir-project)
   ("C-c i l" . 'inf-elixir-send-line)
   ("C-c i r" . 'inf-elixir-send-region)
   ("C-c i b" . 'inf-elixir-send-buffer)
   ("C-c i R" . 'inf-elixir-reload-module)))

(use-package wgsl-ts-mode
  :mode ("\\.wgsl\\'" . wgsl-ts-mode)
  :init
  ;; https://github.com/acowley/wgsl-ts-mode
  (autoload 'wgsl-ts-mode "wgsl-ts-mode" nil t))

(use-package gptel
  :init
  (setq
   ;;gptel-log-level 'info
   gptel-expert-commands t
   gptel-model 'qwen2.5-coder
   gptel-backend (gptel-make-openai "llama-cpp"
                   :stream t
                   :protocol "http"
                   :host "localhost:8080"
                   :models '(qwen2.5-coder))))


(use-package sql
  :defer t
  :config
  ;; https://github.com/duckdb/duckdb/discussions/8099
  (require 'sql-duckdb)

  (defun sql-lint-region (start end)
    (interactive "r")
    (shell-command-on-region start end "sqlfluff lint -"))
  (defun sql-fix-region (start end)
    (interactive "r")
    (shell-command-on-region start end "sqlfluff fix -" t t)))

;;;;;;;;;;;;;;;;
;; Keys
;;;;;;;;;;;;;;;;
(keymap-global-set "M-["    "C-u 1 M-v")
(keymap-global-set "M-]"    "C-u 1 C-v")
(keymap-global-set "C-x C-b" #'ibuffer)
(keymap-global-set "C-x f"   #'find-file)
(keymap-global-set "C-g"     #'keyboard-quit-dwim)
(keymap-global-set "C-<tab>" #'completion-at-point)
(keymap-global-set "<f1>"    #'recompile)
(keymap-global-set "S-<f1>"  #'flymake-show-project-diagnostics)
(keymap-global-set "<f2>"    #'flymake-goto-next-error)
(keymap-global-set "S-<f2>"  #'flymake-goto-prev-error)
(keymap-global-set "M-d"     #'delete-word)
(keymap-global-set "M-<backspace>" #'backward-delete-word)

(defvar-keymap my-prefix-transpose-map
  :doc "Prefix keymap for transposes/toggles"
  :name "Transponse"
  "c" #'transpose-chars
  "l" #'transpose-lines
  "w" #'transpose-words
  "x" #'transpose-sexps
  "h" #'hs-toggle-hiding
  "t" #'toggle-truncate-lines)

(defvar-keymap my-prefix-window-map
  :doc "Prefix keymap for windows"
  :name "Window"
  "k"   #'windmove-up
  "j"   #'windmove-down
  "h"   #'windmove-left
  "l"   #'windmove-right
  "d k" #'windmove-delete-up
  "d j" #'windmove-delete-down
  "d h" #'windmove-delete-left
  "d l" #'windmove-delete-right
  "x l" #'split-root-window-right
  "x j" #'split-root-window-below
  "s k" #'windmove-swap-states-up
  "s j" #'windmove-swap-states-down
  "s h" #'windmove-swap-states-left
  "s l" #'windmove-swap-states-right)

(defvar-keymap my-prefix-global-map
  :doc "Prefix keymap entry point"
  "f" #'project-find-file
  "d" #'duplicate-line
  "t" my-prefix-transpose-map
  "w" my-prefix-window-map)

(keymap-set global-map "C-z" my-prefix-global-map)
;;;;;;;;;;;;;;;;
;; END
;;;;;;;;;;;;;;;;
(setq package-selected-packages '(dape eat eglot gptel typst-ts-mode))
